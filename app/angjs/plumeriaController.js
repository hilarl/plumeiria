'use strict';

/* Controllers */

var url = "http://localhost/plumeiria/v2/api/public/"; 

var HomeCtrl = angular.module('PlumeriaAppCtrl',  ['ui.bootstrap']);

HomeCtrl.controller('HomeCtrl', ['$scope', '$http',
  function($scope, $http) {
 	$scope.myInterval = 5000;
  	var slides = $scope.slides = [];
		  $scope.addSlide = function(image, title, description) {
		    var newWidth = 200 + ((slides.length + (25 * slides.length)) % 150);
		    slides.push({
		      image: image,
		      title: title,
		      text: description
		    });
		  };

		    $http.get(url+'gallery/image/list?id=2').success(function(data) {
		      $scope.images = data.data;
		      
				  for(var i = 0; i< data.data.length; i ++){
				  	$scope.addSlide(url+"files/"+data.data[i].file_name, data.data[i].name, data.data[i].description);
				  }
			  });
	  
  }]);

HomeCtrl.controller('PageCtrl', ['$scope', '$routeParams', '$http', 
  function($scope, $routeParams, $http) {

    $scope.page = $routeParams.page;
    $scope.pageName = "Page Not Found";
    $scope.datalist = [{ title : "404", 
    description : "Page Not Found",
    image : 'http://placehold.it/150x150'}];
    $http.get(url+'api/pages?page_name='+$scope.page).success(function(data){
    	// http://localhost/resort_default/api/public/images/f41ad72d5ad76b7e2deffcbee27c08f3.jpg
      if( data.data.length > 0){
    	$scope.datalist = [];
      	for(var i = 0; i < data.data.length; i++){
          if(i ===0){
            $scope.pageName = data.data[i].name
          }
      		$scope.datalist.push({
      			title : data.data[i].name,
      			description : data.data[i].description,
      			image : url+ "images/"+data.data[i].image,
      		});
      	}
      }
    });
  }]);


HomeCtrl.controller('HotelDetailCtrl', ['$scope', '$routeParams',  '$http', function($scope, $routeParams,  $http){
  $scope.myInterval = 5000;
    var slides = $scope.slides = [];
      $scope.addSlide = function(image, title, description) {
        var newWidth = 200 + ((slides.length + (25 * slides.length)) % 150);
        slides.push({
          image: image,
          title: title,
          text: description
        });
      };
      
      $scope.selectItem = function(name){
          $scope.selectedItem  = name;
      };
      
      $scope.rooms = [];
        $scope.selectedItem = 'introduction';
        
        $http.get(url+'api/property-search?property='+$routeParams.hotel).success(function(data){
//            $scope.images = data.data.product.gallery;
                $scope.propertyInformation = data.data;
               loadGalleryImages($http, $scope, data.data.product.gallery.id);
               loadPropertyRooms($http, $scope, data.data.product.id);
        });
        
       
}]);

function loadPropertyRooms($http, $scope, id){
    $http.get(url+'api/property-rooms?property='+id).success(function(data){
        $scope.rooms = data.data;
    });
}
 function loadGalleryImages($http, $scope, id){
    $http.get(url+'gallery/image/list?id='+id).success(function(data) {
      $scope.images = data.data;

      for(var i = 0; i< data.data.length; i ++){
        $scope.addSlide(url+"files/"+data.data[i].file_name, data.data[i].name, data.data[i].description);
      }
    });
}
HomeCtrl.controller('HotelCtrl', ['$scope', '$http', function($scope, $http){
  $scope.countries = [];
  $scope.holidayTypes = [];
  $scope.productTypes = [];
  $scope.selectedCountry = -1;
  $scope.selectedProductType = -1;
  $scope.selectedHolidayType = -1;
  $scope.places = [];

  $scope.countryName = -1;
  $scope.productTypeID = -1;
  $scope.holidayTypeID = -1;
  $scope.url = url;
  var getList = function(){
    $http.get(url+'pl/search/query?h='+$scope.holidayTypeID+'&p='+$scope.productTypeID+'&c='+$scope.countryName).success(function(data){
        $scope.places = data.data;
    });
  };
  getList();
  $scope.selectCountry  = function(countryIndex){
    $scope.selectedCountry = countryIndex;
    $scope.countryName = $scope.countries[countryIndex]['name'];
    getList();
  };
  $scope.selectHolidayType  = function(index){
    $scope.selectedHolidayType = index;
    $scope.holidayTypeID = $scope.holidayTypes[index]['id'];
    getList();
  };
  $scope.selectProductType  = function(index){
    $scope.selectedProductType = index;
    $scope.productTypeID = $scope.productTypes[index]['name'];
    getList();
  };
  $http.get(url+'api/countries').success(function(data){
      $scope.countries = data.data;
  });
  $http.get(url+'api/holiday-types').success(function(data){
      $scope.holidayTypes = data.data;
  });
  $http.get(url+'api/product-types').success(function(data){
      $scope.productTypes = data.data;
  });
// holidayTypes
}]);

