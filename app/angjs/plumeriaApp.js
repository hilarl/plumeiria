'use strict';

/* App Module */

var plumeriaApp = angular.module('plumeriaApp', [
  'ngRoute',
  'ngSanitize',
  'PlumeriaAppCtrl',
   'ui.bootstrap'
]);

plumeriaApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/home', {
        templateUrl: 'partials/home-page.html',
        controller: 'HomeCtrl'
      }).
      when('/page/:page', {
        templateUrl: 'partials/static-page.html',
        controller: 'PageCtrl'
      }).
      when('/hotels', {
        templateUrl: 'partials/hotels-page.html',
        controller: 'HotelCtrl'
      }).
      when('/hotel/:hotel', {
        templateUrl : "partials/hotel-detail.html",
        controller: "HotelDetailCtrl"
      }).
      otherwise({
        redirectTo: '/home'
      });
  }]);